//
//  STPlace.swift
//  SanomaTest
//
//  Created by Norbert Nemes on 2018. 01. 06..
//  Copyright © 2018. Norbert Nemes. All rights reserved.
//

import UIKit
import CoreLocation

class STPlace: NSObject {
    var name: String?
    var location: CLLocation?
    
    init(dictionary: Dictionary<String, Any>) {
        if let nameString = dictionary["name"] as? String {
            self.name = nameString
        }
        if dictionary["lat"] != nil && dictionary["long"] != nil {
            self.location = CLLocation(latitude: dictionary["lat"] as! CLLocationDegrees, longitude: dictionary["long"] as! CLLocationDegrees)
        }
    }
}
