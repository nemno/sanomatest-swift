//
//  STMapViewController.swift
//  SanomaTest
//
//  Created by Norbert Nemes on 2018. 01. 06..
//  Copyright © 2018. Norbert Nemes. All rights reserved.
//

import UIKit
import GoogleMaps

class STMapViewController: UIViewController {
    
    var mapView: GMSMapView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Places"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(closeButtonPressed))
        
        mapView = GMSMapView(frame: self.view.bounds)
        self.view.addSubview(mapView!)
        
        if ContentManager.places != nil && ContentManager.places!.count > 0 {
            let firstPlace = ContentManager.places!.first
            let camera = GMSCameraPosition.camera(withLatitude: firstPlace!.location!.coordinate.latitude, longitude: firstPlace!.location!.coordinate.longitude, zoom: 12.0)
            mapView!.camera = camera
            self.addPins()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func closeButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func addPins() {
        for place in ContentManager.places! {
            let marker = GMSMarker(position: place.location!.coordinate)
            marker.title = place.name
            marker.map = mapView!
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
