//
//  STDetailViewController.swift
//  SanomaTest
//
//  Created by Norbert Nemes on 2018. 01. 06..
//  Copyright © 2018. Norbert Nemes. All rights reserved.
//

import UIKit
import GoogleMaps

class STDetailViewController: UIViewController {

    var place: STPlace?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let camera: GMSCameraPosition
        
        let mapView = GMSMapView(frame: self.view.bounds)
        self.view.addSubview(mapView)
        
        if self.place != nil {
            self.title = place?.name
            camera = GMSCameraPosition.camera(withLatitude: place!.location!.coordinate.latitude, longitude: place!.location!.coordinate.longitude, zoom: 14.0)
            mapView.camera = camera
            let marker = GMSMarker(position: self.place!.location!.coordinate)
            marker.title = self.place!.name
            marker.map = mapView
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
