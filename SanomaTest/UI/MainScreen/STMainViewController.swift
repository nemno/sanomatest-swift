//
//  STMainViewController.swift
//  SanomaTest
//
//  Created by Norbert Nemes on 2018. 01. 06..
//  Copyright © 2018. Norbert Nemes. All rights reserved.
//

import UIKit

class STMainViewController: UIViewController {

    var tableView: UITableView!
    var tableDelegate: STMainTableDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Places"
        
        tableView = UITableView(frame: self.view.bounds)
        tableDelegate = STMainTableDelegate()
        tableDelegate.viewController = self
        tableView.delegate = tableDelegate
        tableView.dataSource = tableDelegate
        tableView.register(STMainTableViewCell.self, forCellReuseIdentifier: "mainTableCell")
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "map"), style: .plain, target: self, action: #selector(mapButtonPressed))

        self.view.addSubview(tableView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        ContentManager.getPlacesWithCompletion { (places, error) in
            if (places != nil && error == nil) {
                self.tableView.reloadData()
            } else {
                let alertController = UIAlertController(title: "OOps!", message: "Something wnt wrong!", preferredStyle: .alert)
                self.present(alertController, animated: true, completion: nil)
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func mapButtonPressed() {
        let mapViewController = STMapViewController(nibName: nil, bundle: nil)
        self.navigationController?.present(UINavigationController(rootViewController: mapViewController), animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
