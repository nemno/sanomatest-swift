//
//  STMainTableViewCell.swift
//  SanomaTest
//
//  Created by Norbert Nemes on 2018. 01. 06..
//  Copyright © 2018. Norbert Nemes. All rights reserved.
//

import UIKit

class STMainTableViewCell: UITableViewCell {

    var place: STPlace? {
        didSet {
            self.textLabel!.text = place!.name;
        }
    }

}
