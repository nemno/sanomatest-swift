//
//  STMainTableDelegate.swift
//  SanomaTest
//
//  Created by Norbert Nemes on 2018. 01. 06..
//  Copyright © 2018. Norbert Nemes. All rights reserved.
//

import UIKit

class STMainTableDelegate: NSObject {
    weak var viewController: STMainViewController?
}

extension STMainTableDelegate: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ContentManager.places == nil {
            return 0
        }
        
        return ContentManager.places!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mainTableCell", for: indexPath)
        let place = ContentManager.places![indexPath.row]
        cell.textLabel?.text = place.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.viewController != nil {
            let detailViewController = STDetailViewController(nibName: nil, bundle: nil)
            detailViewController.place = ContentManager.places![indexPath.row]
            self.viewController?.navigationController?.pushViewController(detailViewController, animated: true)
        }
    }
}

