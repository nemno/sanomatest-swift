//
//  STNetworkManager.swift
//  SanomaTest
//
//  Created by Norbert Nemes on 2018. 01. 06..
//  Copyright © 2018. Norbert Nemes. All rights reserved.
//

import UIKit
import Alamofire

var NetworkManager = STNetworkManager.sharedInstance

let baseURL = "http://www.sanomamdc.com/test_task/"

class STNetworkManager: NSObject {
    
    static let sharedInstance = STNetworkManager()

    public typealias AsyncCompletion = (_ data:Any?, _ error:Error?) -> Void

    
    private override init() {}
    
    func sendRequestWithParameters(urlTail:String, params: Dictionary<String, Any>?, completion: @escaping AsyncCompletion) {
        let request = Alamofire.request(baseURL + urlTail, method: HTTPMethod.get, parameters: params, encoding: JSONEncoding.default, headers: nil)
        request.responseJSON { (response) in
            if let result = response.result.value {
                completion(result, nil)
            } else {
                completion(nil, response.error)
            }
        }
    }
}
