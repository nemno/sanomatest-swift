//
//  STContentManager.swift
//  SanomaTest
//
//  Created by Norbert Nemes on 2018. 01. 06..
//  Copyright © 2018. Norbert Nemes. All rights reserved.
//

import UIKit

var ContentManager = STContentManager.sharedInstance

class STContentManager: NSObject {
    
    static let sharedInstance = STContentManager()

    var places: Array<STPlace>?
    
    public typealias AsyncCompletion = (_ data:Any?, _ error:Error?) -> Void

    private override init() {}
    
    func getPlacesWithCompletion(completion: @escaping AsyncCompletion) {
        if self.places != nil {
            return completion(places, nil)
        } else {
            NetworkManager.sendRequestWithParameters(urlTail: "pois.json", params: nil, completion: { (responseObject, error) in
                if let dataDictionary = responseObject as? Dictionary<String, Any> {
                    if let poisArray = dataDictionary["pois"] as? Array<Dictionary<String, Any>>{
                        self.places = Array<STPlace>()

                        for poiDictionary in poisArray {
                            let place = STPlace(dictionary: poiDictionary)
                            self.places?.append(place)
                        }
                        
                        return completion(self.places, nil)
                    }
                }
            })
        }
    }
}
