//
//  STNavigationManager.swift
//  SanomaTest
//
//  Created by Norbert Nemes on 2018. 01. 06..
//  Copyright © 2018. Norbert Nemes. All rights reserved.
//

import UIKit

var NavigationManager = STNavigationManager.sharedInstance

class STNavigationManager: NSObject {
    static let sharedInstance = STNavigationManager()

    var window: UIWindow?
    var rootViewController: UIViewController?
    
    private override init() {
        rootViewController = UINavigationController(rootViewController: STMainViewController(nibName: nil, bundle: nil))
         UINavigationBar.appearance().tintColor = UIColor.black
    }
}
